/*  Author: ALCAL� VALERA, DANIEL
 *  Practice: Pr�ctica 1 Expresiones Regulares
 */
package practices;

import java.util.Scanner;

import regexp.Comparator;

/**
 * First practice of 'Language Processors', <strong>University of Le�n</strong>.
 * @author Daniel
 *
 */
public class PracticeOne {

	/**
	 * Ask you for some input text until you write #SALIR. It analyzes the input and detect if known.
	 */
	public static void execute() {
		boolean exit = false;
		
		System.out.println("+---------------------------+");
		System.out.println("|         PR�CTICA 1        |");
		System.out.println("+---------------------------+");
		System.out.println("Para salir escriba #SALIR\n");
		
		do {
			System.out.print("Introduzca una cadena: ");
			String input = new Scanner(System.in).nextLine();
			
			if (input.contains("#SALIR")) {
				System.out.println("\nAlcal� Valera, Daniel - Procesadores del Lenguaje");
				exit = true;
			} else {
				if (Comparator.isDNI(input)) {
					System.out.println("La cadena es un DNI.");
				} else if (Comparator.isNIE(input)) {
					System.out.println("La cadena es un NIF.");
				} else if (Comparator.isRealNumberWithExponent(input)) {
					System.out.println("La cadena es un n�mero real con (o sin) exponente.");
				} else if (Comparator.isDomain(input)) {
					System.out.println("La cadena es un nombre de dominio.");
				} else if (Comparator.isEmail(input)) {
					System.out.println("La cadena es un correo electr�nico.");
				} else if (Comparator.isFunctionName(input)) {
					System.out.println("La cadena es el nombre de una funci�n.");
				} else {
					System.out.println("   [ERROR] La cadena no cumple ninguna expresi�n regular.");
				}
			}
		} while (!exit);
	}
}

