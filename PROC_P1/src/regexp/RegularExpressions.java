/*  Author: ALCAL� VALERA, DANIEL
 *  Practice: Pr�ctica 1 Expresiones Regulares
 */
package regexp;

/**
 * Regular Expression Constants to use in the first practice.
 * @author Daniel
 *
 */
public class RegularExpressions {
	
	// ".es" ".com" ".net" ".org" ".pizza"
	private static final String DOT_DOMAIN = "\\.(es|com|info|net|org|pizza|taxi|barcelona|restaurante)$";
	// "a" "nombre.de.deominio.facil" "nombre.corto"
	private static final String DOMAIN_SENTENCE = "([a-z0-9]+)(\\.[a-z0-9]+)*";
	
	public static final String DNI = "\\d{8}[A-HJ-NP-TV-Z]";
	public static final String NIE = "[X-Z]\\d{7}[A-HJ-NP-TV-Z]";
	public static final String REAL_NUMBER_WITH_EXPONENT = "-{0,1}[0-9]*(\\.[0-9]+([e][0-9]+){0,1}){0,1}";
	public static final String DOMAIN = "^" + DOMAIN_SENTENCE + DOT_DOMAIN;
	public static final String EMAIL = "^([a-z0-9]+)([\\._-][a-z0-9]+)*@" + DOMAIN_SENTENCE + DOT_DOMAIN;
	public static final String FUNCTION_NAME = "^(_|[a-z]){1}[a-zA-Z0-9_]*";
}
