/*  Author: ALCAL� VALERA, DANIEL
 * 
 */
package initializer;

import practices.PracticeOne;

/**
 * Initializes the first practice.
 * @author Daniel
 *
 */
public class Initializer {

	public static void main(String[] args) {
		PracticeOne.execute();
	}

}
